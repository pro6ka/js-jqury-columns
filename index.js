function SubDomainsWrapper(columns, parent, wrapper, wrapSelector) {
	this.construct(columns, parent, wrapper, wrapSelector);
}

SubDomainsWrapper.prototype.construct = function(columns, parent, wrapper, wrapSelector) {
	this.columns = columns;
	this.parent = parent;
	this.wrapper = wrapper;
	this.wrapSelector = wrapSelector;
};

SubDomainsWrapper.prototype.getWrappers = function() {
	return $(`${this.parent}:visible`).find(this.wrapper);
};

SubDomainsWrapper.prototype.countWrappers = function() {
	return this.getWrappers().length;
};

SubDomainsWrapper.prototype.columnLength = function() {
	return this.getNoModulo() / this.columns;
};

SubDomainsWrapper.prototype.hasModulo = function(countWrappers) {
	return countWrappers % this.columns;
};

SubDomainsWrapper.prototype.getNoModulo = function() {
	let wrappersLength = this.countWrappers();
	while (this.hasModulo(wrappersLength) !== 0) {
		wrappersLength++;
	}
	return wrappersLength;
};

SubDomainsWrapper.prototype.getSelectors = function() {
	let selectors = [];
	for (var i = 1; i < this.columns + 1; i++) {
		selectors.push(`${this.wrapSelector}${i}`);
	}
	return selectors;
}

SubDomainsWrapper.prototype.setWrapSelector = function() {
	const wrappers = this.getWrappers();
	const selectors = this.getSelectors();
	const columnLength = this.columnLength();
	wrappers.each(function(i, item) {
		let selectorKeyPrepared = i / columnLength;
		if (i == 0) {
			selectorKey = 0;
		} else if (Number.isInteger(selectorKeyPrepared)) {
			selectorKey = selectorKeyPrepared;
		} else {
			selectorKey = Math.ceil(i / columnLength) -1;
		}
		$(item).addClass(selectors[selectorKey]);
	})
};

SubDomainsWrapper.prototype.wrap = function() {
	const selectors = this.getSelectors();
	selectors.forEach(function(selector, _i) {
		$(`.${selector}`).wrapAll('<div class="sub-domains__column"/>');
	});
};

$(function subDomainsWrapper() {
	const wrapper = new SubDomainsWrapper(4, '.sub-domains', '.sub-domains__wrapper', 'column-');
	wrapper.setWrapSelector();
	wrapper.wrap();
});
